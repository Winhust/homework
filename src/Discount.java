import java.util.HashMap;
import java.util.List;
import java.util.Locale;

class checkout {
    String code;
    double value;

    public checkout(String code, double value) {
        this.code = code;
        this.value = value;
    }
}

class payment {
    double cost;
    double total;
    double ship;

    public payment(double total, double cost, double ship) {
        this.total = total;
        this.cost = cost;
        this.ship = ship;
    }
}

public class Discount {
    String code;
    double value;

    public Discount(String code, double value) {
        this.code = code;
        this.value = value;
    }

    static boolean compare(Discount obj1, checkout obj2) {
        return obj2.code.equals((obj1.code).toUpperCase());

    }

    static boolean compare1(Discount obj1, payment obj2) {
        double discount2 = obj2.ship + obj2.cost - obj2.total;
        return obj1.value == discount2;

    }

    public static void main(String[] args) {
        Discount discount1 = new Discount("testing", 5);
        checkout checkout1 = new checkout("TESTING", 5);
        payment payment1 = new payment(9.99, 10, 4.99);
        boolean res = compare(discount1, checkout1);
        boolean res1 = compare1(discount1, payment1);
        System.out.println("discount code hop le: " + res);
        System.out.println(" Kiem tra value discount: " + res1);
    }
}




